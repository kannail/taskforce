CREATE DATABASE campustrack;
USE campustrack;

CREATE TABLE `FreeLancers`
(
 `id`       int NOT NULL ,
 `login`    varchar(20) NOT NULL ,
 `password` varchar(45) NOT NULL ,

PRIMARY KEY (`id`)
);

CREATE TABLE `Helpers`
(
 `id`     int NOT NULL ,
 `helper` int NOT NULL ,
 `role`   varchar(45) NOT NULL ,

PRIMARY KEY (`id`),
KEY `fkIdx_26` (`helper`),
CONSTRAINT `FK_25` FOREIGN KEY `fkIdx_26` (`helper`) REFERENCES `FreeLancers` (`id`)
);

CREATE TABLE `Services`
(
 `id`           int NOT NULL ,
 `projectName`  varchar(45) NOT NULL ,
 `productOwner` int NOT NULL ,
 `Helpers`      int NULL ,
 `priceAsk`     int NULL ,
 `openSource`   binary NOT NULL ,

PRIMARY KEY (`id`),
KEY `fkIdx_18` (`productOwner`),
CONSTRAINT `FK_17` FOREIGN KEY `fkIdx_18` (`productOwner`) REFERENCES `FreeLancers` (`id`),
KEY `fkIdx_34` (`Helpers`),
CONSTRAINT `FK_33` FOREIGN KEY `fkIdx_34` (`Helpers`) REFERENCES `Helpers` (`id`)
);


CREATE TABLE `Subsciption`
(
 `user`     int NOT NULL ,
 `Service`  int NOT NULL ,
 `feedback` varchar(45) NOT NULL ,
 `note`     varbinary(2) NOT NULL ,

PRIMARY KEY (`user`, `Service`),
KEY `fkIdx_39` (`user`),
CONSTRAINT `FK_38` FOREIGN KEY `fkIdx_39` (`user`) REFERENCES `Users` (`id`),
KEY `fkIdx_43` (`Service`),
CONSTRAINT `FK_42` FOREIGN KEY `fkIdx_43` (`Service`) REFERENCES `Services` (`id`)
);

CREATE TABLE `Users`
(
 `id`       int NOT NULL AUTO_INCREMENT,
 `login`    varchar(20) NOT NULL ,
 `password` varchar(45) NOT NULL ,

PRIMARY KEY (`id`)
);

//Creation user pour connexion php
CREATE USER 'mysql'@'localhost' IDENTIFIED BY 'stillnopassword';
GRANT ALL PRIVILEGES ON * . * TO 'mysql'@'localhost';
FLUSH PRIVILEGES;
